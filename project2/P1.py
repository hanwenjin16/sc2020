"""Scientific Computation Project 2, part 1
Your CID here:01196712
"""


def flightLegs(Alist,start,dest):
    """
    Question 1.1
    Find the minimum number of flights required to travel between start and dest,
    and  determine the number of distinct routes between start and dest which
    require this minimum number of flights.
    Input:
        Alist: Adjacency list for airport network
        start, dest: starting and destination airports for journey.
        Airports are numbered from 0 to N-1 (inclusive) where N = len(Alist)
    Output:
        Flights: 2-element list containing:
        [the min number of flights between start and dest, the number of distinct
        jouneys with this min number]
        Return an empty list if no journey exist which connect start and dest
    """
    #The adjacency list Here is actually defined like a dictionary, i cna change it easily. 
    N=len(Alist)
    """I am planning to find the length of the adjacency list, 
    if it is a dictionary, then it might be difficult"""
    """Create a dictionary to tell if a node is explored"""
    L1=[-100]*N
    #L1 stores the number of flights to this node
    import collections
    Q=collections.deque([start])
    L1[start]=0
    while len(Q)>0:
        n=Q.popleft()
        routev=L1[n]+1
        """the number of route to 
        is the route to the last node plus 1"""
        for v in Alist[n]:#This is an O(1) operation
            if L1[v]==-100:
                Q.append(v)
                L1[v]=routev
            if v==dest:
                Flights=[routev,0]#why initiate to 0? 
                #because it will search all neighbours including v
                for w in Alist[dest]:
                    if L1[w]==routev-1:
                    #if the adjacent node of Q is already explored, 
                    #and the flight is routev-1, then it must 
                    #also be a correct flight route. 
                        Flights[1]+=1
                return Flights
    return []
    


def safeJourney(Alist,start,dest):
    """
    Question 1.2 i)
    Find safest journey from station start to dest
    Input:
        Alist: List whose ith element contains list of 2-element tuples. The first element
        of the tuple is a station with a direct connection to station i, and the second element is
        the density for the connection.
    start, dest: starting and destination stations for journey.

    Output:
        Slist: Two element list containing safest journey and safety factor for safest journey
    """
    Inf=float('inf')
    """Inf is infinity"""
    Edict={}
    Udict={start:[None,0]}
    """The Udict is stored in form of {position:[parent node, S]}, for start, the parent node
    is none"""
    while len(Udict)>0:
        """Assign infinity to Smin to ensure that Smin get updated"""
        Smin=Inf
        for N in Udict:
            S=Udict[N][1]
            if S<Smin:
                Smin=S
                Nmin=N
        for a,d in Alist[Nmin]:
            S=max(Smin,d)
            """S is the safety factor through path Smin"""
            if a in Edict:
                continue
            elif a in Udict:
                """Check if there is a safer path"""
                if S<Udict[a][1]:
                    Udict[a]=[Nmin,S]
                """Since we found a safer path, update the path of a in Udict"""
            else:
                Udict[a]=[Nmin,S]
                """Add node a to Udict"""
        Edict[Nmin]=Udict.pop(Nmin)
        if Nmin==dest:
            """If the element to pop is destination, then reconstruct the path and return it"""
            path=reconstructPath(Edict,start,dest)
            return [path, Smin]

    return [[],None]


def shortJourney(Alist,start,dest):
    """
    Question 1.2 ii)
    Find shortest journey from station start to dest. If multiple shortest journeys
    exist, select journey which goes through the smallest number of stations.
    Input:
        Alist: List whose ith element contains list of 2-element tuples. The first element
        of the tuple is a station with a direct connection to station i, and the second element is
        the time for the connection (rounded to the nearest minute).
    start, dest: starting and destination stations for journey.

    Output:
        Slist: Two element list containing shortest journey and duration of shortest journey
    """
    Inf=float('inf')
    Edict={}
    """Udict is stored in form of {position:[parent node, nodes encountered, total time]}"""
    Udict={start:[None,0,0]}
    while len(Udict)>0:
        tmin=Inf#Initially this value should be defined to be much larger than the biggest S in this problem
        for N in Udict:
            t=Udict[N][2]
            if t<tmin:
                tmin=t
                Nmin=N
        for m , w in Alist[Nmin]:
            tnew=tmin+w#the time for alternative path, possiblely faster than the provitional time in Udict.
            if m in Edict.keys():
                continue
            elif m in Udict.keys():
                #check if there is a faster or shorter path
                if tnew<Udict[m][1]:
                    Udict[m]=[Nmin,tnew]
                elif tnew==Udict[m][1]:
                    if (Udict[Nmin][1]+1)<Udict[m][1]:
                        Udict[m]=[Nmin,Udict[Nmin][1]+1,tnew]
            else:
                Udict[m]=[Nmin,Udict[Nmin][1]+1,tnew]
        Edict[Nmin]=Udict.pop(Nmin)#put the minimum node in Udict into Edict
        if Nmin==dest:
            path=reconstructPath(Edict,start,dest)
            return [path, Edict[dest][2]]
    else:
        return [[],None]

def reconstructPath(Edict, start, end):
    """

    :Edict: The explored dictionary, the first element in value is the parent node
    (if parent node is set to different position, it will produce error)
    :start: The starting node
    :end: The end node
    :returns: path to the start node

    """
    import collections
    if end not in Edict:
        print("end point not explored")
        return []
    path=collections.deque([end])
    while path[0] != start:
        parent=Edict[path[0]][0]
        path.appendleft(parent)
    #convert deque to list
    path=list(path)
    return path
    


def cheapCycling(SList,CList):
    """
    Question 1.3
    Find first and last stations for cheapest cycling trip
    Input:
        Slist: list whose ith element contains cheapest fare for arrival at and
        return from the ith station (stored in a 2-element list or tuple)
        Clist: list whose ith element contains a list of stations which can be
        cycled to directly from station i
    Stations are numbered from 0 to N-1 with N = len(Slist) = len(Clist)
    Output:
        stations: two-element list containing first and last stations of journey
    """
    #Add code here
    Inf=float('inf')
    N=len(SList)
    L1=[0]*N
    import collections
    def bfs(List, start,L1):
        """
        Input:
            start: starting position
            L1: the list that stores whether a node is explored
            List: adjacent List
        Returns:
            It label L1 directly in the fucntion
            The nodes and cost of cheapest arrival and return
            if only one node, None and Infinity"""
        group= []#modify as needed
        Q=collections.deque([start])
        while len(Q)>0:
            n=Q.popleft()
            for v in List[n]:
                if L1[v]==0:
                    L1[v]=1
                    group.append(v)
                    Q.append(v)
        #now we find the minimum cost in this group
        Arr=Inf
        Na=None
        Ret=Inf
        Nr=None
        if len(group)==1:
            return Arr,Ret,Na,Nr
        else:
            for i in group:
                if SList[i][0]<Arr:
                    Arr=SList[i][0]
                    Na=i
                if SList[i][1]<Ret:
                    Ret=SList[i][1]
                    Nr=i
        """If the minimum cost and return appear in the same node,
        Then have to investigate the second minimum"""
        if Na==Nr:
            Arr2=Inf
            Ret2=Inf
            Na2=None
            Nr2=None
            for i in group:
                if i==Na:
                    continue
                if SList[i][0]<Arr2:
                    Arr2=SList[i][0]
                    Na2=i
                if SList[i][1]<Ret2:
                    Ret2=SList[i][1]
                    Nr2=i
            if Arr+Ret2<Arr2+Ret:
                return Arr,Ret2,Na,Nr2
            else:
                return Arr2,Ret,Na2,Nr
        return Arr,Ret,Na,Nr
    """Initialist the global mimimun cost for arrival and return"""
    Arrm=Inf
    Retm=Inf
    Nam=None
    Nrm=None
    for i in range(N):
        if L1[i]==0:
            """If this node is unexplored"""
            Arr,Ret,Na,Nr=bfs(CList, i,L1)
            if Arr+Ret<Arrm+Retm:
                Arrm=Arr
                Retm=Ret
                Nam=Na
                Nrm=Nr
                

    stations=[Nam,Nrm] #modify
    return stations

if __name__=='__main__':
    #add code here if/as desired
    """To test Flightlegs"""
    #Alist=[[1,2,6],[0,3],[0,3],[1,2,6],[5],[4],[0,3]]
    #Flights=flightLegs(Alist,0,3)
    #print(Flights)
    """To test Safejourney"""
    #Alist2=[[(1,100)],[(5,2),(2,3)],[(1,3),(5,1),(3,1)],[(2,1),(4,2)],[(5,5),(6,1)],[(1,2),(4,5),(2,1)],[(4,1)]]
    #Journey=safeJourney(Alist2,6,4)     
    #print(Journey)
    """To test shortJourney"""
    #Alist2=[[(1,100)],[(5,2),(2,3)],[(1,3),(5,1),(3,1)],[(2,1),(4,2)],[(5,5),(6,1)],[(1,2),(4,5),(2,1)],[(4,1)]]
    #Journey=shortJourney(Alist2,6,5)     
    #print(Journey)
    """To test cheapCycling"""
    SList=[[1,3],[4,9],[2,7],[3,10],[2,12]]
    CList=[[1],[0,2],[1,2],[4],[3]]
    ans=cheapCycling(SList,CList)
    print(ans)
