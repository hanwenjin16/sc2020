"""Scientific Computation Project 2, part 2
Your CID here:
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import scipy
from scipy import sparse as sp


def rwgraph(G,i0=0,M=100,Nt=100):
    """ Question 2.1
    Simulate M Nt-step random walks on input graph, G, with all
    walkers starting at node i0
    Input:
        G: An undirected, unweighted NetworkX graph
        i0: intial node for all walks
        M: Number of walks
        Nt: Number of steps per walk
    Output: X: M x Nt+1 array containing the simulated trajectories
    """
    np.random.seed(103)
    X = np.zeros((M,Nt+1))
    X[:,0]=i0
    for i in range(Nt):
        for j in range(M):
            X[j,i+1]=np.random.choice(G.adj[X[j,i]])
    return X

def rwgraph_analyze1(G,NtorM):
    """Analyze simulated random walks on
    Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    def goToNode(vec):
        """Convert a list of position of particle to which node have how many 
        particles

        :vec: simulation results 
        :returns: How many particle each node has

        """
        A=np.zeros(2000)
        for i in range(2000):
            particles=(vec==i).sum()
            A[i]=particles
        return A
    if NtorM==0:
        """=0 means analyse Nt"""
        #First compute the node with maximum degree
        degrees=np.asarray(list(G.degree(range(2000))))[:,1]
        i0=np.where(degrees == np.amax(degrees))[0][0]
        x=np.arange(2000)
        X=rwgraph(G,i0,160000,16)
        for Nts in [2,4,8,16]:
            y=goToNode(X[:,Nts])
            Label='Nt='+str(Nts)+' node i0='+str(y[i0]/160000)
            plt.scatter(x[5:2000],y[5:2000]/160000,.3,label=Label)
        Label='equilibrium, node i0='+str(degrees[i0]/16000)
        plt.scatter(x[5:2000],degrees[5:2000]/degrees.sum(),.3,label=Label)
        plt.xlabel('Node')
        plt.ylabel('Fraction of particles')
        plt.legend()
        plt.show()
    
    """Here is the code to analyse the variation with M"""
    #First compute the node with maximum degree
    if NtorM==1:
        """=1 means analyse M"""
        degrees=np.asarray(list(G.degree(range(2000))))[:,1]
        i0=np.where(degrees == np.amax(degrees))[0][0]
        x=np.arange(2000)
        for Ms in (160,1600,16000,160000):
            X=rwgraph(G,i0,Ms,32)
            y=goToNode(X[:,32])
            Label="M="+str(Ms)
            plt.scatter(x,y/Ms,.3,label=Label)
        Label='equilibrium'
        plt.scatter(x,degrees/degrees.sum(),.3,label=Label)
        plt.legend()
        plt.xlabel("node")
        plt.ylabel("Fraction of particles")
        plt.show()
    
    return None #modify as needed

def rwgraph_analyze2(G):
    """Analyze similarities and differences
    between simulated random walks and linear diffusion on
    Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    N=G.number_of_nodes()
    degrees=np.asarray(list(G.degree(range(N))))[:,1]
    imax=np.where(degrees == np.amax(degrees))[0][0]
    Q=np.diag(np.asarray(list(G.degree(range(N))))[:,1])
    A=nx.to_numpy_matrix(G)
    L=Q-A
    Ls=np.identity(N)-np.dot(np.linalg.inv(Q),A)
    Lst=np.identity(N)-np.dot(A,np.linalg.inv(Q))
    i0=np.zeros((N,1))
    i0[imax][0]=1
    t=100
    """set t to a large value"""
    labels=['L','Ls','Ls^T']
    labelid=0
    for operator in [L,Ls,Lst]:
        it=getit(operator,i0,t)
        plt.plot(it,label=labels[labelid],linewidth=.3)
        labelid+=1
    plt.legend()
    plt.xlabel('node')
    plt.ylabel('value')
    plt.show()

def getit(L,i0,t):
    expL=scipy.linalg.expm(np.asarray(-t*L))
    it=np.dot(expL,i0)
    return it

def modelA(G,x=0,i0=0.1,beta=1.0,gamma=1.0,tf=1,Nt=1000):
    """
    Question 2.2
    Simulate model A

    Input:
    G: Networkx graph
    x: node which is initially infected with i_x=i0
    i0: magnitude of initial condition
    beta,gamma: model parameters
    tf,Nt: Solutions are computed at Nt time steps from t=0 to t=tf (see code below)

    Output:
    iarray: N x Nt+1 Array containing i across network nodes at
                each time step.
    """

    N = G.number_of_nodes()
    iarray = np.zeros((N,Nt+1))
    tarray = np.linspace(0,tf,Nt+1)
    #Add code here
    def RHS(y,t):
        """Compute RHS of modelA at time t
        input: y should be a size N array
        A:the adjacency matrix
        output: dy, also a size N array corresponding to dy/dt
        
        Discussion: add discussion here
        """
        dy=-beta*y+gamma*np.multiply(scipy.sparse.csc_matrix.dot(A,y), 1-y)
        return dy
    y=np.zeros(N)
    y[x]=i0
    A=nx.to_scipy_sparse_matrix(G)
    iarray=odeint(RHS, y, tarray)
    #Add code here

    return iarray


def transport(G):
    """Analyze transport processes (model A, model B, linear diffusion)
    on Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    def modelB(G,Ls, alpha,tf,Nt,x,i0):
        """Simulate Model B
        input:G: the graph
        Ls: the sparse matrix of L
        alpha: parameter
        tf:final time
        Nt:number of steps
        x:initial position
        i0: initial concentration
        """
        N=G.number_of_nodes()
        iarray=np.zeros((2*N,Nt+1))
        tarray=np.linspace(0,tf,Nt+1)
        def RHS(y,t):
            RHS=np.zeros(2*N)
            RHS[0:N]=y[N:2*N]
            RHS[N:2*N]=alpha*scipy.sparse.csc_matrix.dot(Ls,y[0:N])-alpha*np.multiply((y[0:N]),np.sum(Ls,axis=0))
            return RHS
        N=G.number_of_nodes()
        y=np.zeros(2*N)
        y[x+N]=i0
        iarray=odeint(RHS, y, tarray)
        return iarray
    degrees=np.asarray(list(G.degree()))[:,1]
    imax=np.where(degrees == np.amax(degrees))[0][0]
    N=G.number_of_nodes()
    """Numpy matrix of L is needed because expm of sparse matrix is very expensive"""
    A=nx.to_numpy_matrix(G)
    Q=np.diag(np.asarray(list(G.degree()))[:,1])
    L=Q-A
    """Now we construct the sparse version of L"""
    As=nx.to_scipy_sparse_matrix(G,format='csc')
    Qs=scipy.sparse.diags(degrees)
    Ls=Qs-As
    """first we investigate the behaviour of node imax with increasing time"""
    iarray=modelB(G,Ls,-0.01,100,1000000,imax,0.1)
    plt.plot(np.linspace(0,100,1000001),iarray[:,imax+N],label='modelB')
    iarray2=modelA(G,x=imax,i0=0.1,beta=.5,gamma=.1,tf=100,Nt=1000000)
    plt.plot(np.linspace(0,100,1000001),iarray2[:,imax],label='modelA')
    i=np.zeros(N)
    i[imax]=1
    Linear_diffusion_imax=[]
    ts=[0,.1,.2,.4,.6,.8,1,12,14,16,18,20,30,40,50,60,70,80,90,100]
    for t in ts:
        ans=getit(L,i,t)
        Linear_diffusion_imax.append(ans[imax])
    plt.plot(ts,Linear_diffusion_imax,label='Linear diffusion')
    plt.xlabel('t')
    plt.ylabel('value of i in the maximum degree node')
    plt.legend()
    plt.show()
    """Next we investigate the effect of changing the initial condition"""
    iarray=modelA(G,x=imax,i0=0.1,beta=.5,gamma=.1,tf=100,Nt=1000000)
    """We put the particles to imax+1"""
    iarray2=modelA(G,x=imax+1,i0=0.1,beta=.5,gamma=.1,tf=100,Nt=1000000)
    plt.plot(np.linspace(0,100,1000001),(iarray[:,imax]-iarray2[:,imax]),label='modelA')
    iarray=modelB(G,Ls, -0.01,100,1000000,imax,0.1)
    iarray2=modelB(G,Ls, -0.01,100,1000000,imax+1,0.1)
    plt.plot(np.linspace(0,100,1000001),(iarray[:,imax+N]-iarray2[:,imax+N]),label='modelB')
    i=np.zeros(N)
    i2=np.zeros(N)
    i[imax]=.1
    i2[imax+1]=.1
    difference=[]
    for t in ts:
        ans=getit(L,i,t)
        ans2=getit(L,i2,t)
        difference.append(ans[imax]-ans2[imax])
    plt.plot(ts,difference,label='Linear diffusion')
    plt.xlabel('t')
    plt.ylabel('difference in i value in maximum node')
    plt.legend()
    plt.show()
    """Next we can show modelB and linear diffusion conserves the total particles"""
    iarray=modelA(G,x=imax,i0=.1,beta=.5,gamma=.1,tf=1,Nt=1000000)
    iarray=iarray.sum(axis=1)
    plt.plot(np.linspace(0,1,1000001),iarray,label='modelA')
    iarray=modelB(G,Ls,-0.01,1,1000000,imax,1)
    iarray=iarray[:,N:2*N]
    iarray=iarray.sum(axis=1)
    plt.plot(np.linspace(0,1,1000001),iarray,label='modelB')
    ts=np.linspace(0,1,10)
    i=np.zeros(N)
    i[imax]=.5
    sums=[]
    for t in ts:
        ans=getit(L,i,t)
        sums.append(ans.sum())
    plt.plot(ts,sums,label='Linear diffusion')
    plt.legend()
    plt.xlabel('node')
    plt.ylabel('sum of i values')
    plt.show()
    iarray=modelA(G,x=imax,i0=.1,beta=.5,gamma=.1,tf=150,Nt=1000000)
    plt.plot(np.linspace(0,150,1000001),iarray[:,99]/np.amax(iarray[:,99]),label='modelA')
    iarray2=modelB(G,Ls, -0.01,150,1000000,imax+1,0.1)
    plt.plot(np.linspace(0,150,1000001),(iarray2[:,2*N-1]/np.amax(iarray2[:,2*N-1])),label='modelB')
    ts=[0,.1,.2,.4,.6,.8,1,12,14,16,18,20,30,40,50,60,70,80,90,100,150]
    Linear_diffusion=np.zeros(len(ts))
    index=0
    for t in ts:
        ans=getit(L,i,t)
        Linear_diffusion[index]=ans[99]
        index+=1
    plt.plot(ts,Linear_diffusion/np.amax(Linear_diffusion),label='Linear diffusion')
    plt.legend()
    plt.xlabel('t')
    plt.ylabel('i[99]/max_t(i[99]')
    plt.show()



    return None






if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    """Initialise the graph"""
    G=nx.barabasi_albert_graph(2000,4,seed=923)
    """Analyse the effect of Nt"""
    #rwgraph_analyze1(G,0)
    """Analyse the effect of M"""
    #rwgraph_analyze1(G,1)
    """part 2.1 iii, linear diffusion"""
    rwgraph_analyze2(G)
    """Call transport"""
    G1=nx.barabasi_albert_graph(100,5,seed=114514)
    transport(G1)
