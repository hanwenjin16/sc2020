"""Scientific Computation Project 4
Your CID here:
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
from scipy.special import expit #sigmoid function
import matplotlib.pyplot as plt
import pandas as pd


def data(fname='sc2020/project4/project4.csv'):
    """The function will load human mobility data from the input file and
    convert it into a weighted undirected NetworkX Graph.
    Each node corresponds to a country represented by its 3-letter
    ISO 3166-1 alpha-3  code. Each edge between a pair of countries is
    weighted with the number of average daily trips between the two countries.
    The dataset contains annual trips for varying numbers of years, and the daily
    average is computed and stored below.
    """

    df = pd.read_csv(fname,header=0) #Read dataset into Pandas dataframe, may take 1-2 minutes


    #Convert dataframe into D, a dictionary of dictionaries
    #Each key is a country, and the corresponding value is
    #a dictionary which has a linked country as a key
    #and a 2-element list as a value.  The 2-element list contains
    #the total number of trips between two countries and the number of years
    #over which these trips were taken
    D = {}
    for index, row in df.iterrows():
         c1,c2,yr,N = row[0],row[1],row[2],row[3]
         if len(c1)<=3:
             if c1 not in D:
                 D[c1] = {c2:[N,1]}
             else:
                 if c2 not in D[c1]:
                     D[c1][c2] = [N,1]
                 else:
                     Nold,count = D[c1][c2]
                     D[c1][c2] = [N+Nold,count+1]


    #Create new dictionary of dictionaries which contains the average daily
    #number of trips between two countries rather than the 2-element lists
    #stored in D
    Dnew = {}
    for k,v in D.items():
        Dnew[k]={}
        for k2,v2 in v.items():
            if v2[1]>0:
                v3 = D[k2][k]
                w_ave = (v2[0]+v3[0])/(730*v2[1])
                if w_ave>0: Dnew[k][k2] = {'weight':w_ave}

    G = nx.from_dict_of_dicts(Dnew) #Create NetworkX graph

    return G


def network(G,inputs=()):
    """
    Analyze input networkX graph, G
    Use inputs to provide any other needed information.
    """
    nx.draw(G,node_size=1,edge_size=.1)
    plt.show()
    DG=nx.degree_histogram(G)
    plt.semilogy(DG)
    plt.xlabel('degree')
    plt.ylabel('number of countries')
    plt.show()
    Gnp=nx.to_numpy_matrix(G)
    if np.all(Gnp==Gnp.T):
        print('G is symmetric')
    else:
        print('G is not symmetric')
    Gmax=np.amax(Gnp)
    Nmax1,Nmax2=np.where(Gnp==Gmax)
    Nodes=np.array(G.node())
    print('The nodes for the maximul weight edge is %s %s' %(Nodes[Nmax1[0]],Nodes[Nmax1[1]]))
    Clist1,Clist2=np.where(Gnp>0.33*Gmax)
    string=('The countries with at least a third the weight of %s to %s are '%(Nodes[Nmax1[0]],Nodes[Nmax1[1]]))
    for C1,C2 in zip(Nodes[Clist1],Nodes[Clist2]):
        if (C2+' to '+C1) in string:
            pass
        else:
            string+=C1+' to '+C2+', '
    print(string)
    #Now we investigate the countries with less than 100 degree
    Gdegree=np.array(G.degree())[:,1].astype(int)
    Countries=np.where(Gdegree<100)
    string='The countries with less than 100 degrees are '
    for C in Nodes[Countries]:
        string+=C+' '
    print(string)
    total_weight=Gnp.sum(axis=0)
    Nodelist=np.array(G.nodes)
    v=np.zeros(196)
    v[:]=total_weight[0,:]
    print(v.shape)
    plt.figure(figsize=(20,3))
    plt.xticks(rotation=90,fontsize=2.5)
    plt.bar(Nodelist,v)
    plt.show()
    return None



def modelBH(G,x=0,i0=0.1,alpha=0.45,beta=0.3,gamma=1e-3,eps=1.0e-6,eta=8,tf=20,Nt=1000):
    """
    Simulate model Brockmann & Helbing SIR model

    Input:
    G: Weighted undirected Networkx graph
    x: node which is initially infected with j_x=j0
    j0: magnitude of initial condition
    alpha,beta,gamma,eps,eta: model parameters
    tf,Nt: Solutions are computed at Nt time steps from t=0 to t=tf (see code below)

    Output:
    tarray: size Nt+1 array
    jarray: Nt+1 x N array containing j across the N network nodes at
                each time step.
    sarray: Nt+1 x N array containing s across network nodes at each time step
    """
    Fmn=nx.to_numpy_matrix(G)
    Flist=np.sum(Fmn,axis=1)
    Fn,AA=np.meshgrid(Flist,np.zeros_like(Flist))
    Pmn=Fmn/Fn
    print(Pmn)
    N = G.number_of_nodes()
    tarray = np.linspace(0,tf,Nt+1)
    y0=np.zeros(2*N)
    y0[x]=i0
    y0[N:]=1
    def sigma(cnm):
        sigma=np.divide(cnm**eta,(1+cnm**eta))
        return sigma
    def RHS(y,t):
        dy=np.zeros(2*N)
        j=y[:N]
        s=y[N:]
        print(gamma*(np.dot(j,Pmn)-np.dot(Pmn,j)))
        dj=alpha*np.multiply(np.multiply(s,j),sigma(j/eps))-beta*j+gamma*(np.dot(j,Pmn)-np.dot(Pmn,j))
        ds=-alpha*np.multiply(np.multiply(s,j),sigma(j/eps))+gamma*(np.dot(s,Pmn)-np.dot(Pmn,s))
        dy[:N]=dj
        dy[N:]=ds
        return dy
    #constuct the initial condition
    ans=odeint(RHS,y0,tarray)
    return ans







    #return tarray,jarray,sarray


def analyze(G,inputs=()):
    """Compute effective distance matrix and
    analyze simulation results
    Input:
        G: Weighted undirected NetworkX graphs
        inputs: can be used to provide additional needed information
    Output:
        D: N x N effective distance matrix (a numpy array)

    """

    D = None

    return D



if __name__=='__main__':
    #Add code below to call network and analyze so that they generate the figures
    #in your report.
    G = data()
    network(G)
