""" Your college id here:01196712
    Template code for part 2, contains 3 functions:
    codonToAA: returns amino acid corresponding to input amino acid
    DNAtoAA: to be completed for part 2.1
    pairSearch: to be completed for part 2.2
"""
def DNAtoAA(S):
    """Convert genetic sequence contained in input string, S,
    into string of amino acids corresponding to the distinct
    amino acids found in S and listed in the order that
    they appear in S
    """
    #we put the table directly here, because each time we call the CondonToAA function, there is a cost of initialisation of table
    table = {
    	'ATA':'i', 'ATC':'i', 'ATT':'i', 'ATG':'m',
    	'ACA':'t', 'ACC':'t', 'ACG':'t', 'ACT':'t',
    	'AAC':'n', 'AAT':'n', 'AAA':'k', 'AAG':'k',
    	'AGC':'s', 'AGT':'s', 'AGA':'r', 'AGG':'r',
        'CTA':'l', 'CTC':'l', 'CTG':'l', 'CTT':'l',
    	'CCA':'p', 'CCC':'p', 'CCG':'p', 'CCT':'p',
    	'CAC':'h', 'CAT':'h', 'CAA':'q', 'CAG':'q',
    	'CGA':'r', 'CGC':'r', 'CGG':'r', 'CGT':'r',
    	'GTA':'v', 'GTC':'v', 'GTG':'v', 'GTT':'v',
    	'GCA':'a', 'GCC':'a', 'GCG':'a', 'GCT':'a',
    	'GAC':'d', 'GAT':'d', 'GAA':'e', 'GAG':'e',
    	'GGA':'g', 'GGC':'g', 'GGG':'g', 'GGT':'g',
    	'TCA':'s', 'TCC':'s', 'TCG':'s', 'TCT':'s',
    	'TTC':'f', 'TTT':'f', 'TTA':'l', 'TTG':'l',
    	'TAC':'y', 'TAT':'y', 'TAA':'_', 'TAG':'_',
    	'TGC':'c', 'TGT':'c', 'TGA':'_', 'TGG':'w',
    }
    AA=''
    l=len(S)
    for i in range(0,l-2,3):
        condon=S[i:i+3]
        aa=table[condon]
        if aa not in AA :
            AA=AA+aa
    return AA


def pairSearch(L,pairs):
    """Find locations within adjacent strings (contained in input list,L)
    that match k-mer pairs found in input list pairs. Each element of pairs
    is a 2-element tuple containing k-mer strings
    """
    L_L=len(L)
    locations=[]
    for p in range(len(pairs)):
        k1,k2=pairs[p][0],pairs[p][1]
        k=len(k1)
        for i in range(L_L-1):
            matches=RKserach(L[i],k1)
            for l in matches:
                if L[i+1][l:l+k]==k2:
                    locations.append([l,i,p])
    return locations

def char2base4(S):
    """Convert gene test_sequence
    string to list of ints
    """
    table={'A':'0','C':'1','G':'2','T':'3',}
    L=''
    for s in S:
        L+=table[s]
    return L

def heval(L,Base,Prime):
    """Convert list L to base-10 number mod Prime
    where Base specifies the base of L
    """
    f = 0
    for l in L[:-1]:
        f = Base*(int(l)+f)
    h = (f + (int(L[-1]))) % Prime
    return h

def RKserach(string,kmer):
    matches=[]
    L_s,k=len(string),len(kmer)
    kmer_number=heval(char2base4(kmer),4,997)
    S=char2base4(string)
    coef=heval('1'+'0'*(k-1),4,997)
    string_number=heval(char2base4(string[:k]),4,997)
    for j in range(L_s-k-1):
        if string_number==kmer_number:
            if string[j:j+k]==kmer:
                matches.append(j)
        string_number=((string_number-(int(S[j])*coef))*4+int(S[j+k]))%997
    if string[L_s-k:L_s]==kmer:
        matches.append(L_s-k)
    return matches 
if __name__ == "__main__":
    print(DNAtoAA('ATAACCACG'))
    S='ATAATCATAATGTGATGA'
    L=['TATTCGAAG','ATAATATGG','CGGCCATTC']
    pairs=[['TAT','ATA'],['TCG','ATA'],['TGG','TTC']]
    locations=pairSearch(L,pairs)
    print("locations=" )
    print(locations)


