""" Your college id here:01196712
    Template code for part 1, contains 4 functions:
    newSort, merge: codes for part 1.1
    time_newSort: to be completed for part 1.1
    findTrough: to be completed for part 1.2
"""

def create_plot(N,k,repeats=10,n_or_k=True):
    #This create plot function gets a list of variable 1, and calculate the time needed as a function of variable 1.
    #It output a time and standard error. 
    #Where standard error is calculated using sigma/sqrt(n), sigma is standard deviation, n is the 
    #number of repetition. 
    #The last variable n_or_k determines whether n is a variable(True), or k is a variable(False)
    """First we need a for loop for each element in variable. 
    Then within the loop, we update time_data, which store the time spend on each loop, 
    After the second loop, we calculate the mean and standard error and put them into t_m and SE
    """
    import time
    import numpy as np
    import matplotlib.pyplot as plt
    mean,SE=[],[]
    if n_or_k==True:
        L=len(N)
    else:
        L=len(k)
    for i in range(L):
        #update of time data
        time_data=[]
        #for loop
        for repeat in range(repeats):
            #calculate time by reading the time before and after the function
            #We update the list
            if n_or_k==True:
                #get the i th element of N
                Ni=N[i]
                X=np.random.randint(1,Ni,Ni)
                ki=k
            else:
                X=np.random.randint(1,N,N)
                ki=k[i]
            t1=time.time()
            newSort(X,ki)
            t2=time.time()
            time_data.append(t2-t1)
        #calculate the mean and standard error
        mean.append(np.mean(time_data))
        SE.append(np.std(time_data)/(repeats**.5))
    return mean,SE


def newSort(X,k=0):
    """Given an unsorted list of integers, X,
        sort list and return sorted list
    """
    n = len(X)
    if n==1:
        return X
    elif n<=k:
        for i in range(n-1):
            ind_min = i
            for j in range(i+1,n):
                if X[j]<X[ind_min]:
                    ind_min = j
            X[i],X[ind_min] = X[ind_min],X[i]
        return X
    else:
        L = newSort(X[:n//2],k)
        R = newSort(X[n//2:],k)
        return merge(L,R)


def merge(L,R):
    """Merge 2 sorted lists provided as input
    into a single sorted list
    """
    M = [] 
    indL,indR = 0,0 
    nL,nR = len(L),len(R)
    for i in range(nL+nR):
        if L[indL]<R[indR]:
            M.append(L[indL])
            indL = indL + 1
            if indL>=nL:
                M.extend(R[indR:])
                break
        else:
            M.append(R[indR])
            indR = indR + 1
            if indR>=nR:
                M.extend(L[indL:])
                break
    return M

def time_newSort():
    import time
    import timeit
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import rc
    fig,ax = plt.subplots(2, 2,sharey=False,sharex=False,figsize=[16,12])
    #Here a 2x2 plot was created
    N=range(5,41)
    NPlot=[ax[0,0],ax[0,1]]#top two axis was allocated to the plot for N
    #the plot of variation of time with N at fixed k was plotted
    #Here k=0 and 5 were chosen, where k=5 usually create a minimum. 
    k=0
    for i in NPlot:
        for k in [0,5]:
            #errorbar was calculated using 5 repeats and use 
            #error =std/sqrt(n)
            time,SE=create_plot(N,k,5,True)
            i.errorbar(N,time,SE).set_label('k=%i'%k)
        i.set_ylabel('Time ($t$)')
        i.set_xlabel('List length $N$ (no unit)')
        if i==ax[0,0]:
            i.set_title('(a)')
        else:
            i.set_title('(b)')
        i.legend()
        N=range(0,100000,10000)
        #here N was updated to plot a wider range
    #now a graph for variation of time with k was plotted with N fixed to 10000
    kPlot=[ax[1,0],ax[1,1]]#bottom two axia was allocated to k plot
    k=range(0,50)
    N=10000
    for j in kPlot:
        time,SE=create_plot(N,k,5,False)
        j.errorbar(k,time,SE)
        j.set_ylabel('Time ($t$)')
        j.set_xlabel('k(no unit)')
        if j==ax[1,0]:
            j.set_title('(c)')
        else:
            j.set_title('(d)')
        i.legend()
        k=range(0,10000,100)
    plt.show()
    fig.savefig('fig1.pdf')
    
def findTrough(L):
    """Find and return a location of a trough in L
    """
    l=len(L)
    if L[0]<L[1]:
        return 0
    elif L[l-2]<L[l-1]:
        return l
    else:
        for i in range(1,l-1):
            if L[i-1]<=L[i] and L[i]<=L[i+1]:
                return i
    return -l-1

if __name__=='__main__':
    time_newSort()
    L=[6,5,4,0,3,2,1,0,10,11]
    I=findTrough(L)
    print(I)
    
