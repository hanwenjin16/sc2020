"""Scientific Computation Project 3, part 2
Your CID here:01196712
"""
from scipy.signal import welch
from scipy.spatial.distance import pdist
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy import sparse as sp
import scipy
import time
def microbes(phi,kappa,mu,L = 1024,Nx=1024,Nt=1201,T=600,display=False):
    """
    Question 2.2
    Simulate microbe competition model

    Input:
    phi,kappa,mu: model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of f when true

    Output:
    f,g: Nt x Nx arrays containing solution
    """

    #generate grid
    L = 1024
    x = np.linspace(0,L,Nx)
    dx = x[1]-x[0]
    dx2inv = 1/dx**2

    def RHS(y,t,k,r,phi,dx2inv):
        #RHS of model equations used by odeint

        n = y.size//2

        f = y[:n]
        g = y[n:]

        #Compute 2nd derivatives
        d2f = (f[2:]-2*f[1:-1]+f[:-2])*dx2inv
        d2g = (g[2:]-2*g[1:-1]+g[:-2])*dx2inv

        #Construct RHS
        R = f/(f+phi)
        dfdt = d2f + f[1:-1]*(1-f[1:-1])- R[1:-1]*g[1:-1]
        dgdt = d2g - r*k*g[1:-1] + k*R[1:-1]*g[1:-1]
        dy = np.zeros(2*n)
        dy[1:n-1] = dfdt
        dy[n+1:-1] = dgdt

        #Enforce boundary conditions
        a1,a2 = -4/3,-1/3
        dy[0] = a1*dy[1]+a2*dy[2]
        dy[n-1] = a1*dy[n-2]+a2*dy[n-3]
        dy[n] = a1*dy[n+1]+a2*dy[n+2]
        dy[-1] = a1*dy[-2]+a2*dy[-3]

        return dy


    #Steady states
    rho = mu/kappa
    F = rho*phi/(1-rho)
    G = (1-F)*(F+phi)
    y0 = np.zeros(2*Nx) #initialize signal
    y0[:Nx] = F
    y0[Nx:] = G + 0.01*np.cos(10*np.pi/L*x) + 0.01*np.cos(20*np.pi/L*x)

    t = np.linspace(0,T,Nt)

    #compute solution
    print("running simulation...")
    y = odeint(RHS,y0,t,args=(kappa,rho,phi,dx2inv),rtol=1e-6,atol=1e-6)
    f = y[:,:Nx]
    g = y[:,Nx:]
    print("finished simulation")
    if display:
        plt.figure()
        plt.contour(x,t,f)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of f')


    return f,g


def newdiff(f,h):
    """
    Question 2.1 i)
    Input:
        f: array whose 2nd derivative will be computed
        h: grid spacing
    Output:
        d2f: second derivative of f computed with compact fd scheme
    """
    def getF(N,h):
        I=(-2*a-b/2-c*2/9)/h**2
        A=a/h**2
        B=(b/4)/h**2
        C=(c/9)/h**2
        F=np.zeros_like(f)
        F[0]=np.dot(np.array([145/12,-76/3,29/2,-4/3,1/12]),f[0:5])
        F[1]=np.dot(np.array([A,I,A,B,C]),f[0:5])+np.dot(np.array([C,B]),f[N-3:N-1])
        F[2]=np.dot(np.array([B,A,I,A,B,C]),f[0:6])+C*f[N-2]
        F[N-3]=np.dot(np.array([C,B,A,I,A,B]),f[N-6:N])+C*f[1]
        F[N-2]=np.dot(np.array([C,B,A,I,A]),f[N-5:N])+np.dot(np.array([B,C]),f[1:3])
        F[N-1]=np.dot(np.array([1/12,-4/3,29/2,-76/3,145/12]),f[N-5:N])
        MI=I*f[3:N-3]
        MA1=A*f[2:N-4]
        MA2=A*f[4:N-2]
        MB1=B*f[1:N-5]
        MB2=B*f[5:N-1]
        MC1=C*f[6:N]
        MC2=C*f[0:N-6]
        Fp=MI+MA1+MA2+MB1+MB2+MC1+MC2
        F[3:N-3]=Fp
        return F

    def getMb(N):
        Mb=np.zeros([3,N])
        Mb[0,2:]=alpha
        Mb[1,:]=1
        Mb[2,:N-2]=alpha
        Mb[2,N-2]=10
        Mb[0,1]=10
        return Mb
    t1=time.time()
    N=len(f)
    #Coefficients for compact fd scheme
    alpha = 9/38
    a = (696-1191*alpha)/428
    b = (2454*alpha-294)/535
    c = (1179*alpha-344)/2140
    F=getF(N,h)
    Mb=getMb(N)
    d2f=scipy.linalg.solve_banded([1,1],Mb,F)
    dt=time.time()-t1
    return d2f,dt #modify as needed

def analyzefd():
    """
    Question 2.1 ii)
    Add input/output as needed

    """
    def diff(y,h):
        t1=time.time()
        dy2=np.zeros_like(x)
        dy2[1:-1] = (y[2:]-2*y[1:-1]+y[:-2])/h**2
        dy2[0]=(y[-2]-2*y[0]+y[1])/h**2
        dy2[-1]=(y[-3]-2*y[-2]+y[0])/h**2
        dt=time.time()-t1
        return dy2,dt
    col=['red','green','blue']
    i=0
    fig,ax=  plt.subplots(1,2)
    #analyse the effects of bigger k 
    for k in [1,10,100]:
        x=np.linspace(0,2*np.pi,5000)
        y=np.sin(k*x)
        dye=-k**2*np.sin(k*x)
        dy,dt=newdiff(y,x[1]-x[0])
        dy2,dt=diff(y,x[1]-x[0])
        ax[0].semilogy(x[5000//4:-5000//4],np.abs((dye-dy))[5000//4:-5000//4],linewidth=.3,label=('newdiff, k=%i'%k),color=col[i])
        ax[0].semilogy(x[5000//4:-5000//4],np.abs(dye-dy2)[5000//4:-5000//4],linewidth=.3,label=('diff, k=%i'%k),color=col[i],linestyle='dashed')
        ax[1].semilogy(x[0:100],np.abs((dye-dy))[0:100],linewidth=.3,label=('newdiff, k=%i'%k),color=col[i])
        ax[1].semilogy(x[0:100],np.abs(dye-dy2)[0:100],linewidth=.3,label=('diff, k=%i'%k),color=col[i],linestyle='dashed')
        i+=1
    ax[0].set_xlabel('x value')
    ax[1].set_xlabel('x value')
    ax[0].set_ylabel('absolute error')
    ax[0].legend(fontsize='x-small')
    ax[1].legend(fontsize = 'x-small')
    plt.show()
    j=0
    k=50
    col=['red','green','blue']
    fig,ax=  plt.subplots(1,2)
    #analyse the effects of more grids
    for grids in [5000,1000,500]:
        x=np.linspace(0,2*np.pi,grids,endpoint=True)
        y=np.sin(k*x)
        dye=-k**2*np.sin(k*x)
        dy,dt=newdiff(y,x[1]-x[0])
        dy2,dt=diff(y,x[1]-x[0])
        ax[0].semilogy(x[grids//4:-grids//4],np.abs((dye[grids//4:-grids//4]-dy[grids//4:-grids//4])),linewidth=.3,label=('newdiff, grid=%i'%grids),color=col[j])
        ax[0].semilogy(x[grids//4:-grids//4],np.abs(dye[grids//4:-grids//4]-dy2[grids//4:-grids//4]),linewidth=.3,label=('diff, grid=%i'%grids),color=col[j],linestyle='dashed')
        ax[1].semilogy(x[0:grids//50],np.abs(dye-dy)[0:grids//50],linewidth=.3,label=('newdiff, grid=%i'%grids),color=col[j])
        ax[1].semilogy(x[0:grids//50],np.abs(dye-dy2)[0:grids//50],linewidth=.3,label=('diff, grid=%i'%grids),color=col[j],linestyle='dashed')
        j+=1
    ax[0].set_xlabel('x value')
    ax[1].set_xlabel('x value')
    ax[0].set_ylabel('absolute error')
    ax[0].legend(fontsize='x-small')
    ax[1].legend(fontsize = 'x-small')
    plt.show()
    #analyse the time
    t1=[]
    t2=[]
    k=5000
    for grids in np.linspace(500000,5000000,10):
        x=np.linspace(0,2*np.pi,grids,endpoint=True)
        y=np.sin(k*x)
        dye=-k**2*np.sin(k*x)
        dy,dt=newdiff(y,x[1]-x[0])
        t1.append(dt)
        dy2,dt=diff(y,x[1]-x[0])
        t2.append(dt)
    m1,c1=np.polyfit(np.linspace(500000,5000000,10),np.array(t1),1)
    m2,c2=np.polyfit(np.linspace(500000,5000000,10),np.array(t2),1)

    plt.plot(np.linspace(50000,500000,10)*10,t1,label=('newdiff,m=%.2e'%m1))
    plt.plot(np.linspace(50000,500000,10)*10,t2,label=('diff,m=%.2e'%m2))
    plt.legend()
    plt.xlabel('grids')
    plt.ylabel('t')
    plt.show()
    k=200000
    t1=[]
    t2=[]
    e1=[]
    e2=[]
    for grids in np.linspace(500000,5000000,10):
        x=np.linspace(0,2*np.pi,grids,endpoint=True)
        y=np.sin(k*x)
        dye=-k**2*np.sin(k*x)
        dy,dt=newdiff(y,x[1]-x[0])
        err1=np.abs(np.amax((dy-dye)[int(grids//4):int(3*grids//4)]))
        t1.append(dt)
        e1.append(err1)
        x=np.linspace(0,2*np.pi,6*grids,endpoint=True)
        y=np.sin(k*x)
        dye=-k**2*np.sin(k*x)
        dy2,dt=diff(y,x[1]-x[0])
        err2=np.abs(np.amax((dy2-dye)[int(grids//4):int(3*grids//4)]))
        e2.append(err2)
        t2.append(dt)
    plt.semilogy(t1,e1,label='newdiff')
    plt.semilogy(t2,e2,label='diff')
    plt.xlabel('time')
    plt.ylabel('error')
    plt.legend()
    plt.show()

    


    return None #modify as needed


def dynamics():
    """
    Question 2.2
    Add input/output as needed

    """
    #simulate for kappa=1.5
    def getDimension(kappa):
        mu=kappa*0.4
        nt=60000
        t=30000
        f,g=microbes(0.3,kappa,mu,L = 1024,Nx=1024,Nt=nt,T=t,display=False)
        f500=f[nt//2:,500]
        w,P = welch(f500)
        fm = w[P==P.max()]*nt/t
        tau=1/(5*fm)
        dt=t/nt
        Del=int(tau/dt)
        print(Del)
        eps=np.arange(0,5,.5)
        eps=np.exp(eps*np.log(2))*0.005
        A=np.vstack([f500[:-3*Del],f500[Del:-2*Del],f500[2*Del:-1*Del],f500[3*Del:]]).T
        n=len(f500[:-4*Del])
        D=pdist(A)
        C=np.zeros(len(eps))
        for i in range(len(eps)):
            Di=D[D<eps[i]]
            C[i]=Di.size
        m,c=np.polyfit(np.log(eps),np.log(C),1)
        C/= n*n/2
        return m,C,f[nt//2:,:]
    Clist=[]
    mlist=[]
    eps=np.arange(0,5,.5)
    eps=np.exp(eps*np.log(2))*0.005
    flist=[]
    for kappa in [1.5,1.7,2]:
        m,C,f=getDimension(kappa)
        plt.loglog(eps,C,label=('kappa=%.2f , m=%.2f'%(kappa,m)))
        mlist.append(m)
        Clist.append(C)
        flist.append(f)
    plt.legend()
    plt.xlabel('$\epsilon$')
    plt.ylabel('$C(\epsilon)$')
    plt.show()
    nt=60000
    t=30000
    tlist = np.linspace(t/2,t,nt//2)
    xlist=np.linspace(0,1024,1024)
    klist=[1.5,1.7,2]
    kindex=0
    for fi in flist:
        plt.contourf(xlist,tlist,fi)
        plt.colorbar()
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('$\kappa$ =%.2f' %klist[kindex])
        plt.show()
        kindex+=1
    return None #modify as needed

if __name__=='__main__':
    pass
