"""Scientific Computation Project 3, part 1
Your CID here:01196712
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy.integrate import odeint

def hfield(r,th,h,levels=50):
    """Displays height field stored in 2D array, h,
    using polar grid data stored in 1D arrays r and th.
    Modify as needed.
    """
    thg,rg = np.meshgrid(th,r)
    xg = rg*np.cos(thg)
    yg = rg*np.sin(thg)
    plt.figure()
    plt.contourf(xg,yg,h,levels)
    plt.axis('equal')
    plt.show()
    return None

def repair1(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    np.random.seed(seed=1)
    #problem setup
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    S = set()
    for i,j in zip(iK,jK):
            S.add((i,j))

    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))

    #Create lists of indices used during optimization
    mlist = [[] for i in range(a)]
    nlist = [[] for j in range(b)]

    for i,j in zip(iK,jK):
        mlist[i].append(j)
        nlist[j].append(i)

    dA = np.zeros(niter)
    dB = np.zeros(niter)

    for z in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    Bfac = 0.0
                    Asum = 0
                    for j in mlist[m]:
                        Bfac += B[n,j]**2
                        Rsum = 0
                        for k in range(p):
                            if k != n: Rsum += A[m,k]*B[k,j]
                        Asum += (R[m,j] - Rsum)*B[n,j]

                    A[m,n] = Asum/(Bfac+l) #New A[m,n]
                if m<p:
                    Afac=0.0
                    Bsum=0
                    for i in nlist[n]:
                        Afac+=A[i,m]**2
                        Rsum=0
                        for k in range(p):
                            if k!=m: Rsum+=A[i,k]*B[k,n]
                        Bsum+=(R[i,n]-Rsum)*A[i,m]

                    B[m,n]=Bsum/[Afac+l]
                    #Add code here to update B[m,n]
        dA[z] = np.sum(np.abs(A-Aold))
        dB[z] = np.sum(np.abs(B-Bold))
        if z%10==0: print("z,dA,dB=",z,dA[z],dB[z])


    return A,B


def repair2(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R. Efficient and complete version of repair1.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    #problem setup
    np.random.seed(seed=1)
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    S = set()
    for i,j in zip(iK,jK):
            S.add((i,j))
    #Create lists of indices used during optimization
    mlist = np.ones([a,b])
    nlist = np.ones([b,a]) 
    
    mlist2=mlist.copy()
    nlist2=nlist.copy()
    for i,j in zip(aK,bK):
        mlist[i,j]=0
        nlist[j,i]=0
    #Set initial dA,dB
    dA = np.zeros(niter)
    dB = np.zeros(niter)
    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))
    for z in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    jarray=mlist[m,:]
                    Bfac=np.dot(jarray,np.square(B[n,:]))
                    #Now compute Rmj-sum AmkBkj term
                    Rmj=R[m,:]
                    AB=np.dot(A[m,:],B)
                    AB-=A[m,n]*B[n,:]
                    Aarray=Rmj-AB
                    Asum=np.dot(np.multiply(Aarray,jarray),B[n,:])
                    A[m,n] = Asum/(Bfac+l) #New A[m,n]
                if m<p:
                    jarray=nlist[n,:]
                    Afac=np.dot(np.square(A[:,m]),jarray)
                    Rin=R[:,n]
                    AB=np.dot(A,B[:,n])
                    AB-=A[:,m]*B[m,n]
                    Barray=Rin-AB
                    Bsum=np.dot(np.multiply(Barray,jarray),A[:,m])
                    B[m,n]=Bsum/[Afac+l]
                    #Add code here to update B[m,n]
        dA[z] = np.sum(np.abs(A-Aold))
        dB[z] = np.sum(np.abs(B-Bold))
        if z%10==0: print("z,dA,dB=",z,dA[z],dB[z])


    #Add code here

    return A,B


def outwave(r0):
    """
    Question 1.2i)
    Calculate outgoing wave solution at r=r0
    See code/comments below for futher details
        Input: r0, location at which to compute solution
        Output: B, wave equation solution at r=r0

    """
    #Need to ask which data is loaded
    A = np.load('data2.npy')
    r = np.load('r.npy')
    th = np.load('theta.npy')
    Nr,Ntheta,Nt = A.shape
    f=A[0,:,:]
    #Compute the FT of hr
    htil=np.fft.fft2(f)#we denote the transformed H as htil
    w=np.fft.fftfreq(Nt)*Nt*8
    m=np.fft.fftfreq(Ntheta)*Ntheta
    ww,mm=np.meshgrid(w,m)
    H=sp.special.hankel1(mm,ww)
    Hr=sp.special.hankel1(mm,ww*r0)
    C=np.multiply(np.divide(Hr,H),htil)
    C=np.nan_to_num(C)
    B=np.fft.ifft2(C)
    return B

def analyze1():
    """
    Question 1.2ii)
    Add input/output as needed

    """
    """ pi/4 at 36, 3 pi /4 at 108,5 pi/4 at 180 """
    def reorder(data):
        """Reorder the input data such that it 
        runs from N/2 to N-1 back to N/2-1"""
        a,b=data.shape
        b=b//2
        left=data[:,:b]
        right=data[:,b:]
        dnew=np.zeros_like(data)
        dnew[:,b+1:]=left
        dnew[:,:b+1]=right
        return dnew

    data3=np.load('data3.npy')
    th=np.load('theta.npy')
    r = np.load('r.npy')
    hr1=data3[:,36,:]
    plt.contourf(np.arange(0,119,1),r,hr1)
    plt.colorbar()
    plt.xlabel('t')
    plt.ylabel('r')
    plt.show()
    hr2=data3[:,108,:]
    plt.contourf(np.arange(0,119,1),r,hr2)
    plt.colorbar()
    plt.xlabel('t')
    plt.ylabel('r')
    plt.show()
    hr3=data3[:,180,:]
    plt.contourf(np.arange(0,119,1),r,hr3)
    plt.colorbar()
    plt.xlabel('t')
    plt.ylabel('r')
    plt.show()
    fig,ax=plt.subplots(1,3,figsize=(15,3))
    hr1=reorder(hr1)
    hr2=reorder(hr2)
    hr3=reorder(hr3)
    ax[0].contourf(np.arange(0,119,1),r,hr1)
    ax[1].contourf(np.arange(0,119,1),r,hr2)
    ax[2].contourf(np.arange(0,119,1),r,hr3)
    plt.show()

    return None #modify as needed




def reduce(H,n):
    """
    Question 1.3: Construct one or more arrays from H
    that can be used by reconstruct
    Input:
        H: 3-D data array
        n:the number of rows/columns retained in u,s,vh
    Output:
        arrays: a tuple containing the arrays produced from H
    """
    #Add code here
    """We want a svd for the time and theta"""
    u,s,vh=np.linalg.svd(H)
    u=u[:,:,:n]
    s=s[:,:n]
    vh=vh[:,:n,:]
    arrays=(u,s,vh)


    return arrays


def reconstruct(arrays):
    """
    Question 1.3: Generate matrix with same shape as H (see reduce above)
    that has some meaningful correspondence to H
    Input:
        arrays: tuple generated by reduce
    Output:
        Hnew: a numpy array with the same shape as H
    """
    #Add code here
    u=arrays[0]
    s=arrays[1]
    vh=arrays[2]
    n=s.shape[1]
    #first reconstruct U,Vh
    us=u[:,:,:]*s[...,None,:]
    Hnew=np.matmul(us,vh[:,:,:])

    return Hnew


if __name__=='__main__':
    pass
